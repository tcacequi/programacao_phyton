import random


def jogar():
    print("***forca***")

    secret_word = read_words()

    correct_letter = ["_" for letter in secret_word]  # list comprehension

    dead = False
    win = False
    mistake = 0

    while (not dead and not win):
        
        print(correct_letter)
        guess = str.lower(input("Wich letter ?"))

        if (guess in secret_word):
            clock = 0
            for letter in secret_word:
                if (guess == letter):
                    print("found the letter {} in the {} position".format(letter, clock + 1))
                    correct_letter[clock] = letter.lower()
                clock += 1
        else:
            mistake += 1
            print("wrong, {} tries remaining".format(6 - mistake))
            desenha_forca(mistake)

        dead = mistake == 7
        win = "_" not in correct_letter

    if dead == True:
        print("\nthe word was {}".format(secret_word))
        print("\nGame over")
        print("________")
        print("       |")
        print("       |")
        print("       O")
        print("      /|/")
        print("       ^")

    if win == True:
        print("\nthe word was {}".format(secret_word))
        print("\nYou win!!!\n")


def read_words():
    with open("words.txt", "r") as file:
        words = []

        for line in file:
            line = line.strip()
            words.append(line)

    number = random.randrange(0, len(words))
    secret_word = words[number].lower()
    return secret_word

def desenha_forca(mistake):
    print("  _______     ")
    print(" |/      |    ")

    if(mistake == 1):
        print(" |      (_)   ")
        print(" |            ")
        print(" |            ")
        print(" |            ")

    if(mistake == 2):
        print(" |      (_)   ")
        print(" |      \     ")
        print(" |            ")
        print(" |            ")

    if(mistake == 3):
        print(" |      (_)   ")
        print(" |      \|    ")
        print(" |            ")
        print(" |            ")

    if(mistake == 4):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |            ")
        print(" |            ")

    if(mistake == 5):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |       |    ")
        print(" |            ")

    if(mistake == 6):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |       |    ")
        print(" |      /     ")

    if (mistake == 7):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |       |    ")
        print(" |      / \   ")

    print(" |            ")
    print("_|___         ")
    print()

if (__name__ == "__main__"):
    jogar()