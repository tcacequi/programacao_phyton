import random #importando funções

def jogar():

# while (surprise == 0):
#    surprise = round(random.random() * 100)
#surprise = round(surprise)

    clock = 0 #"variaveis"
    points = 1000
    surprise = random.randrange(1, 101)
    print("{}". format(surprise))

    print ("Digite o nível de dificuldade: ") #escolhe dificuldade
    print ("(1) fácil (2) médio (3) dificil")
    nivel = int(input("Defina o nível: "))
    if(nivel == 1):
        clock = 20
    elif(nivel == 2):
        clock = 10
    else:
        clock = 5


    for rodada in range(1, clock):
        print("tentativa {} de {}".format(rodada, clock))
        guess = input("Digite ")
        guess = int(guess)
        print("voce digitou", guess)
        if (guess <= 0 or guess > 100):
            print("Digite um numero maior que 0 e menor que 100")
            continue

        if (surprise == guess):
            print("voce acertou e fez {} pontos".format(points))
            break
        else:
            if (surprise > guess):
                print("numero menor que o chute.")
                lost_points = abs(surprise - guess)
                points = points - lost_points
            elif (surprise < guess):
                print("numero maior que o chute")
                lost_points = abs(surprise - guess)
                points = points - lost_points

if (__name__=="__main__"):
    jogar()