from abc import ABC #abstract base classes

class Programa:
    def __init__(self, nome, ano):
        self._nome = nome
        self.ano = ano
        self._likes = 0

    def likes(self):
        self._likes += 1

    @property
    def like(self):
        return self._likes

    def __str__(self):  # maneira "phytonica" de string
        return f'nome: {self._nome} Likes: {self._likes}'

    def __getitem__(self, item):
        return self._programas[item]

class Serie(Programa):

    def __init__(self, nome, ano, temporadas):
        super().__init__(nome, ano)

        self.temporadas = temporadas

    def __str__(self):  # maneira "phytonica" de string
        return f'nome: {self._nome} - temporadas {self.temporadas} - Likes: {self._likes}'


class Filme(Programa):  # herança
    def __init__(self, nome, ano, duracao):
        super().__init__(nome, ano)
        self.duracao = duracao

    def __str__(self):  # maneira "phytonica" de string
        return f'nome: {self._nome} - ano {self.ano} - duracao {self.duracao} Likes: {self._likes}'


class playlist(list):
    def __init__(self, nome, programas):
        self.nome = nome
        self._programas = programas

    @property
    def listagem(self):
        return self._programas

    def __len__ (self):
        return len(self._programas)


vingadores = Filme('vingadores - guerra infinita', 2018, 160)

Atlanta = Serie('Atlanta', 2020, 3)

filmes_e_series = [Atlanta, vingadores]

playlist_fim_de_semana = playlist('fim de semana', filmes_e_series)

print(f'tamanho da lista {len(playlist)}')

for programa in playlist_fim_de_semana:  # Polimorfismo, buscando dentro do programa
    print(programa)       # e listando independenete do objeto

