class Account:

    def __init__(self, number, owner, value, limit):
        print("\nRunning object...")
        self.__number = number
        self.__owner = owner
        self.__value = value
        self.__limit = limit

    def extract(self):
        print("Saldo {} do titular {}".format(self.__value, self.__owner))

    def deposit(self, money):
        self.__value += money

    def __can_withdraw(self,amount):
        return amount <= (self.__value + self.__limit)

    def withdraw(self, money):
        if (self.__can_withdraw(money)):
            self.__value -= money
        else:
            print ("The value {} exceed your limit".format(money))

    def transfer(self, money, receiver):
        self.withdraw(money)
        receiver.deposit(money)

    @property # possibilita que o metodo seja executado sem os "()"
    def get_data_account(self):  # retorna o mesmo valor da variavel
        return self.__number
    @property
    def get_data_owner(self):
        return self.__owner

    @property
    def value(self):
        return self.__value
    
    @property
    def limit(self):
        return self.__limit

    @limit.setter  # para poder definir sem inserir os "()"
    def limit(self, limit):
        self.__limit = limit

    @staticmethod # objeto que nao precisa da classe
    def bank_code():
        return "001" 
